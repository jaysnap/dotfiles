#!/bin/bash

mv /home/gitpod/.gitconfig /home/gitpod/.gitconfig.bak
ln -s /home/gitpod/.dotfiles/.gitconfig /home/gitpod/.gitconfig
ln -s /home/gitpod/.dotfiles/.bash_aliases /home/gitpod/.bash_aliases
cp /home/gitpod/.dotfiles/hooks.sh /workspace/remote-dev/hooks.sh
echo "source /home/gitpod/.dotfiles/dbt.env" >> /home/gitpod/.bashrc
echo "git config --global core.filemode false" >> /home/gitpod/.bashrc
echo "git config core.fileMode false"  >> /home/gitpod/.bashrc
chmod -R 777 ~/.aws
