#!/bin/bash
if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$HOME/.dotfiles
    mv $HOME/.gitconfig $HOME/.gitconfig.bak
fi

ln -s $DOTFILE_PATH/.gitconfig $HOME/.gitconfig
ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases

# Use Administrator super-powers
# awk '{gsub(/dev-temp-privileged/,"default")};1' $HOME/.aws/config > $HOME/.aws/config.tmp && mv $HOME/.aws/config.tmp $HOME/.aws/config
# awk '{gsub(/DeveloperAccess/,"AdministratorAccess")};1' $HOME/.aws/config > $HOME/.aws/config.tmp && mv $HOME/.aws/config.tmp $HOME/.aws/config

# Install gcloud cli
sudo apt-get --assume-yes update
sudo apt-get --assume-yes install apt-transport-https ca-certificates gnupg && echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && sudo apt-get --assume-yes update && sudo apt-get --assume-yes install google-cloud-cli

# Install bun
# npm install -g bun 
